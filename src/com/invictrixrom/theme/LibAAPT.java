package com.invictrixrom.theme;

public class LibAAPT {
	public static native boolean compileTheme(String manifestPath, String resPath, String outputPath, String targetApk);

	static {
		System.loadLibrary("themeaapt");
	}
}
