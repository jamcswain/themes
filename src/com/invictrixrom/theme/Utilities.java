package com.invictrixrom.theme;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class Utilities {

	public static ArrayList<ThemeInfo> getInstalledThemes(Context context) {
		ArrayList<ThemeInfo> themes = new ArrayList<>();
		List<ApplicationInfo> packages = context.getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
		for (ApplicationInfo packageInfo : packages) {
			try {
				Bundle metaData = packageInfo.metaData;
				String themeName, themeAuthor;
				boolean isThemePackage = metaData.getBoolean("ThemePackage") || false;
				if(isThemePackage) {
					themeName = metaData.getString("Theme_Name");
					themeAuthor = metaData.getString("Theme_Author");
					themes.add(new ThemeInfo(packageInfo.packageName, themeName, themeAuthor));
				}
			} catch(Exception ex) {
				//Swallow
			}
		}
		return themes;
	}

	public static boolean isAppInstalled(Context context, String packageName) {
		List<PackageInfo> appList = context.getPackageManager().getInstalledPackages(PackageManager.GET_CONFIGURATIONS);
		for(PackageInfo info : appList) {
			if(info.packageName.equals(packageName)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isSystemApp(Context context, String packageName) {
		List<PackageInfo> appList = context.getPackageManager().getInstalledPackages(PackageManager.MATCH_SYSTEM_ONLY);
		for(PackageInfo info : appList) {
			if(info.packageName.equals(packageName)) {
				return true;
			}
		}
		return false;
	}

	public static boolean copyAssetFolder(AssetManager assetManager, String fromAssetPath, String toPath) {
		try {
			String[] files = assetManager.list(fromAssetPath);
			new File(toPath).mkdirs();
			boolean res = true;
			for (String file : files)
				if (file.contains("."))
					res &= copyAsset(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
				else 
					res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath) {
		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open(fromAssetPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void copyFile(InputStream in, OutputStream out) throws Exception {
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) != -1){
			out.write(buffer, 0, read);
		}
	}
}
