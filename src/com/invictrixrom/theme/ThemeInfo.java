package com.invictrixrom.theme;

import java.io.Serializable;

public class ThemeInfo implements Serializable {
	private String packageName, themeName, themeAuthor;

	public ThemeInfo(String packageName, String themeName, String themeAuthor) {
		this.packageName = packageName;
		this.themeName = themeName;
		this.themeAuthor = themeAuthor;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getThemeName() {
		return themeName;
	}

	public String getThemeAuthor() {
		return themeAuthor;
	}
}
