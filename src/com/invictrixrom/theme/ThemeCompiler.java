package com.invictrixrom.theme;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.om.InterfacerManager;
import android.content.res.AssetManager;

import java.io.File;
import java.util.ArrayList;

public class ThemeCompiler {
	private Context context;
	private ThemeInfo themeInfo;
	private ThemeCompileCallback callback;

	private boolean applyStyle = false;
	private boolean applyApps = false;
	private boolean applyWallpaper = false;
	private boolean applyLockscreenWallpaper = false;
	private boolean applyFont = false;
	private boolean applyIcons = false;
	private boolean applyBootAnimation = false;
	private boolean applyRingtone = false;
	private boolean applyNotificationSound = false;
	private boolean applyAlarmSound = false;

	private InterfacerManager interfacer;
	private PackageManager packageManager;

	public ThemeCompiler(Context context) {
		this.context = context;
		interfacer = (InterfacerManager) context.getSystemService("interfacer");
		packageManager = context.getPackageManager();
	}

	public void setTheme(ThemeInfo themeInfo) {
		this.themeInfo = themeInfo;
	}

	public void setApplyStyle(boolean applyStyle) {
		this.applyStyle = applyStyle;
	}

	public void setApplyApps(boolean applyApps) {
		this.applyApps = applyApps;
	}

	public void setApplyWallpaper(boolean applyWallpaper) {
		this.applyWallpaper = applyWallpaper;
	}

	public void setApplyLockscreenWallpaper(boolean applyLockscreenWallpaper) {
		this.applyLockscreenWallpaper = applyLockscreenWallpaper;
	}

	public void setApplyFont(boolean applyFont) {
		this.applyFont = applyFont;
	}

	public void setApplyIcons(boolean applyIcons) {
		this.applyIcons = applyIcons;
	}

	public void setApplyBootAnimation(boolean applyBootAnimation) {
		this.applyBootAnimation = applyBootAnimation;
	}

	public void setApplyRingtone(boolean applyRingtone) {
		this.applyRingtone = applyRingtone;
	}

	public void setApplyNotificationSound(boolean applyNotificationSound) {
		this.applyNotificationSound = applyNotificationSound;
	}

	public void setApplyAlarmSound(boolean applyAlarmSound) {
		this.applyAlarmSound = applyAlarmSound;
	}

	public void setCallback(ThemeCompileCallback callback) {
		this.callback = callback;
	}

	public void compile() {
		AssetManager assets = ThemeUtils.getAssetsForTheme(context, themeInfo);
		ArrayList<String> overlaysToInstall = new ArrayList<>();
		ArrayList<String> overlaysToEnable = new ArrayList<>();
		ArrayList<String> overlaysToCompile = new ArrayList<>();
		String[] overlays = { "" };
		try {
			overlays = assets.list("overlays");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if(applyStyle) {
			for(int i=0; i<overlays.length; i++) {
				if(Utilities.isAppInstalled(context, overlays[i]) && Utilities.isSystemApp(context, overlays[i])) {
					overlaysToCompile.add(overlays[i]);
				}
			}
		}
		if(applyApps) {
			for(int i=0; i<overlays.length; i++) {
				if(Utilities.isAppInstalled(context, overlays[i]) && !Utilities.isSystemApp(context, overlays[i])) {
					overlaysToCompile.add(overlays[i]);
				}
			}
		}
		for(String overlay : overlaysToCompile) {
			String unsignedOverlay = "/sdcard/themes/" + overlay + ".apk";
			String signedOverlay = "/sdcard/themes/" + overlay + "-signed.apk";

			String manifestPath = ThemeUtils.generateManifest(context, themeInfo, overlay);

			String resPath = ThemeUtils.extractOverlayForTheme(context, themeInfo, overlay);
			new File("/sdcard/themes/").mkdir();
			long startTime = System.nanoTime();
			String baseApk = "";
			try {
				baseApk = packageManager.getApplicationInfo(overlay, 0).sourceDir;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			LibAAPT.compileTheme(manifestPath, resPath, unsignedOverlay, baseApk);
			long totalTime = System.nanoTime() - startTime;
			System.out.println("BLUG: Compiling " + overlay + " overlay took " + ((float) totalTime/1000000.0f) + " milliseconds");
			//Sign
			startTime = System.nanoTime();
			ThemeUtils.signApk(context, unsignedOverlay, signedOverlay);
			totalTime = System.nanoTime() - startTime;
			System.out.println("BLUG: Signing " + overlay + " overlay took " + ((float) totalTime/1000000.0f) + " milliseconds");

			overlaysToInstall.add(signedOverlay);
			overlaysToEnable.add(overlay + "." + themeInfo.getPackageName());

			new File(manifestPath).delete();
			new File(resPath).delete();
		}
		interfacer.installPackage(overlaysToInstall);


		//Either this needs to be done way later, wait for a callback, or offloaded to OMS
		//Right now it installs, then tries to immediately enable the overlay before the package is even fully installed
		try {
			Thread.sleep(1000);
		} catch (Exception ex) {}
		interfacer.enableOverlay(overlaysToEnable);


		callback.compileDone(true);
		new File("/sdcard/themes/").delete();
	}
}
