package com.invictrixrom.theme;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends Activity implements ThemeInstallCallback {

	private ArrayList<ThemeInfo> installedThemes;
	private LinearLayout themeList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		themeList = findViewById(R.id.theme_list);

		installedThemes = Utilities.getInstalledThemes(this);

		getApplicationContext().registerReceiver(new PackageInstallReceiver(this), new IntentFilter(Intent.ACTION_PACKAGE_ADDED));
		getApplicationContext().registerReceiver(new PackageUninstallReceiver(this), new IntentFilter(Intent.ACTION_PACKAGE_FULLY_REMOVED));

		for(ThemeInfo theme : installedThemes) {
			addThemeToLayout(theme);
		}
	}

	@Override
	public void themePackageInstalled(ThemeInfo themeInfo) {
		for(ThemeInfo theme : installedThemes) {
			if(theme.getPackageName().equals(themeInfo.getPackageName())) {
				installedThemes.remove(theme);
				removeThemeFromLayout(theme);
				break;
			}
		}
		installedThemes.add(themeInfo);
		addThemeToLayout(themeInfo);
	}

	@Override
	public void packageUninstalled(String packageName) {
		for(ThemeInfo theme : installedThemes) {
			if(theme.getPackageName().equals(packageName)) {
				installedThemes.remove(theme);
				break;
			}
		}
	}

	private void addThemeToLayout(ThemeInfo themeInfo) {
		LinearLayout themeLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.theme_card, null);
		TextView themePackage = themeLayout.findViewById(R.id.theme_package);
		TextView themeName = themeLayout.findViewById(R.id.theme_name);
		TextView themeAuthor = themeLayout.findViewById(R.id.author_name);
		themePackage.setText(themeInfo.getPackageName());
		themeName.setText(themeInfo.getThemeName());
		themeAuthor.setText(themeInfo.getThemeAuthor());

		themeLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ViewGroup card = (ViewGroup) view;
				String themePackage = ((TextView) card.findViewById(R.id.theme_package)).getText().toString();
				for(ThemeInfo theme : installedThemes) {
					if(theme.getPackageName().equals(themePackage)) {
						Intent detailsIntent = new Intent(MainActivity.this, ThemeActivity.class);
						detailsIntent.putExtra("themeinfo", theme);
						MainActivity.this.startActivity(detailsIntent);
						break;
					}
				}
			}
		});

		themeList.addView(themeLayout);
	}

	private void removeThemeFromLayout(ThemeInfo themeInfo) {
		for(int i=0; i<themeList.getChildCount(); i++) {
			LinearLayout child = (LinearLayout) themeList.getChildAt(i);
			TextView themePackage = child.findViewById(R.id.theme_package);
			if(themePackage.getText().toString().equals(themeInfo.getPackageName())) {
				themeList.removeView(child);
				break;
			}
		}
	}
}
