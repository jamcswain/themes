package com.invictrixrom.theme;

public interface ThemeInstallCallback {
	public void themePackageInstalled(ThemeInfo themeInfo);
	public void packageUninstalled(String packageName);
}
