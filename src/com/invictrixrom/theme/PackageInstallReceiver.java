package com.invictrixrom.theme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class PackageInstallReceiver extends BroadcastReceiver {

	ThemeInstallCallback callback;

	public PackageInstallReceiver(ThemeInstallCallback callback) {
		this.callback = callback;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String packageName = intent.getData().getEncodedSchemeSpecificPart();
		try {
			ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_META_DATA);
			Bundle metaData = appInfo.metaData;

			String themeName, themeAuthor;
			boolean isThemePackage = metaData.getBoolean("ThemePackage") || false;
			if(isThemePackage) {
				themeName = metaData.getString("Theme_Name");
				themeAuthor = metaData.getString("Theme_Author");
				callback.themePackageInstalled(new ThemeInfo(packageName, themeName, themeAuthor));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
