package com.invictrixrom.theme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PackageUninstallReceiver extends BroadcastReceiver {

	ThemeInstallCallback callback;

	public PackageUninstallReceiver(ThemeInstallCallback callback) {
		this.callback = callback;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String packageName = intent.getData().getEncodedSchemeSpecificPart();
		callback.packageUninstalled(packageName);
	}
}
