package com.invictrixrom.theme;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;

public class ThemeActivity extends Activity implements ThemeCompileCallback {

	private ThemeInfo themeInfo;
	private LinearLayout themeableList;
	private CheckBox style, apps, wallpapers, lockscreenWallpapers, fonts, icons, bootAnimation, ringtones, notificationSounds, alarmSounds;
	private TextView installButton, themeDetailsName, authorDetailsName;
	private ImageView themeDetailsImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.theme_details);

		themeInfo = (ThemeInfo) getIntent().getSerializableExtra("themeinfo");

		themeableList = findViewById(R.id.theme_details_themeable);

		style = findViewById(R.id.style);
		apps = findViewById(R.id.apps);
		wallpapers = findViewById(R.id.wallpapers);
		lockscreenWallpapers = findViewById(R.id.lockscreen_wallpapers);
		fonts = findViewById(R.id.fonts);
		icons = findViewById(R.id.icons);
		bootAnimation = findViewById(R.id.boot_animation);
		ringtones = findViewById(R.id.ringtones);
		notificationSounds = findViewById(R.id.notification_sounds);
		alarmSounds = findViewById(R.id.alarm_sounds);

		setupCheckboxes();

		installButton = findViewById(R.id.install_button);

		//TODO preview images
		themeDetailsImage = findViewById(R.id.theme_details_image);

		themeDetailsName = findViewById(R.id.theme_details_name);
		themeDetailsName.setText(themeInfo.getThemeName());

		authorDetailsName = findViewById(R.id.author_details_name);
		authorDetailsName.setText(themeInfo.getThemeAuthor());

		installButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ThemeCompiler compiler = new ThemeCompiler(ThemeActivity.this);
				compiler.setTheme(themeInfo);
				compiler.setApplyStyle(style.isChecked());
				compiler.setApplyApps(apps.isChecked());
				compiler.setApplyWallpaper(wallpapers.isChecked());
				compiler.setApplyLockscreenWallpaper(lockscreenWallpapers.isChecked());
				compiler.setApplyFont(fonts.isChecked());
				compiler.setApplyIcons(icons.isChecked());
				compiler.setApplyBootAnimation(bootAnimation.isChecked());
				compiler.setApplyRingtone(ringtones.isChecked());
				compiler.setApplyNotificationSound(notificationSounds.isChecked());
				compiler.setApplyAlarmSound(alarmSounds.isChecked());
				compiler.setCallback(ThemeActivity.this);
				compiler.compile();
			}
		});
	}

	@Override
	public void compileDone(boolean success) {
		//TODO Error handling
	}

	private void setupCheckboxes() {
		wallpapers.setVisibility((ThemeUtils.hasWallpaper(this, themeInfo)) ? View.VISIBLE:View.GONE);
		lockscreenWallpapers.setVisibility((ThemeUtils.hasLockscreenWallpaper(this, themeInfo)) ? View.VISIBLE:View.GONE);
		fonts.setVisibility((ThemeUtils.hasFont(this, themeInfo)) ? View.VISIBLE:View.GONE);
		icons.setVisibility((ThemeUtils.hasIcons(this, themeInfo)) ? View.VISIBLE:View.GONE);
		bootAnimation.setVisibility((ThemeUtils.hasBootAnimation(this, themeInfo)) ? View.VISIBLE:View.GONE);
		ringtones.setVisibility((ThemeUtils.hasRingtone(this, themeInfo)) ? View.VISIBLE:View.GONE);
		notificationSounds.setVisibility((ThemeUtils.hasNotificationSound(this, themeInfo)) ? View.VISIBLE:View.GONE);
		alarmSounds.setVisibility((ThemeUtils.hasAlarmSound(this, themeInfo)) ? View.VISIBLE:View.GONE);
	}
}
