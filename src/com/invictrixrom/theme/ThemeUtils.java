package com.invictrixrom.theme;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;

import com.android.apksig.ApkSigner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.util.ArrayList;

public class ThemeUtils {

	public static boolean hasBootAnimation(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("bootanimation.zip").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasFont(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("font.ttf").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasIcons(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("icons/").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasWallpaper(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("wallpapers/wallpaper.png").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasLockscreenWallpaper(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("wallpapers/lockscreen_wallpaper.png").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasRingtone(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("sounds/ringtone.ogg").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasNotificationSound(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("sounds/notification.ogg").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean hasAlarmSound(Context context, ThemeInfo theme) {
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
			return assets.list("sounds/alarm.ogg").length > 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static AssetManager getAssetsForTheme(Context context, ThemeInfo theme) {
		try {
			return context.getPackageManager().getResourcesForApplication(theme.getPackageName()).getAssets();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String generateManifest(Context context, ThemeInfo themeInfo, String overlay) {
		String baseManifest = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\" package=\"PNAME\" android:versionCode=\"VCODE\" android:versionName=\"VNAME\">\n<overlay android:priority=\"1\" android:targetPackage=\"TNAME\" />\n<application android:label=\"ANAME\" allowBackup=\"false\" android:hasCode=\"false\">\n</application>\n</manifest>";

		String filePath = "";
		try {
			PackageInfo themePkgInfo = context.getPackageManager().getPackageInfo(themeInfo.getPackageName(), PackageManager.GET_CONFIGURATIONS);
			baseManifest = baseManifest.replaceAll("PNAME", overlay + "." + themeInfo.getPackageName());
			baseManifest = baseManifest.replaceAll("VCODE", themePkgInfo.versionCode + "");
			baseManifest = baseManifest.replaceAll("VNAME", themePkgInfo.versionName + "");
			baseManifest = baseManifest.replaceAll("TNAME", overlay);
			baseManifest = baseManifest.replaceAll("ANAME", overlay + "." + themeInfo.getPackageName());
			System.out.println("BLUG: Manifest: " + baseManifest);
			filePath = context.getFilesDir().getAbsoluteFile() + "/AndroidManifest.xml";
			PrintStream out = new PrintStream(new FileOutputStream(filePath));
			out.print(baseManifest);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return filePath;
	}

	public static String extractOverlayForTheme(Context context, ThemeInfo themeInfo, String overlay) {
		String fileName = context.getFilesDir().getAbsoluteFile() + "/res-" + overlay + "-" + themeInfo.getPackageName();
		try {
			AssetManager assets = context.getPackageManager().getResourcesForApplication(themeInfo.getPackageName()).getAssets();
			Utilities.copyAssetFolder(assets, "overlays/" + overlay, fileName);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return fileName;
	}

	public static boolean signApk(Context context, String apkPath, String outputPath) {
		AssetManager assets = context.getAssets();
		try {
			InputStream keyIn = assets.open("private.key.pk8");
			InputStream certIn = assets.open("public.certificate.x509.pem");

			X509Certificate cert = CryptoUtils.readPublicKey(certIn);
			PrivateKey key = CryptoUtils.readPrivateKey(keyIn);
			ArrayList<X509Certificate> certificates = new ArrayList<>();
			certificates.add(cert);

			ApkSigner.SignerConfig signerConfig = new ApkSigner.SignerConfig.Builder("SINS", key, certificates).build();
			ArrayList<ApkSigner.SignerConfig> signerConfigs = new ArrayList<>();
			signerConfigs.add(signerConfig);

			ApkSigner.Builder apkSigner = new ApkSigner.Builder(signerConfigs);
			apkSigner.setInputApk(new File(apkPath))
				.setOutputApk(new File(outputPath))
				.setMinSdkVersion(Build.VERSION.SDK_INT)
				.build()
				.sign();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

}
