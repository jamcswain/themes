#include <Bundle.h>
#include <ZipEntry.h>
#include <Main.h>

#include "libaapt.h"

jboolean compileTheme(char* manifestPath, char* resPath, char* outputPath, char* targetApk) {
	Bundle bundle;

	bundle.setCompressionMethod(ZipEntry::kCompressStored);
	bundle.setCommand(kCommandPackage);
	bundle.setForce(true);
	bundle.setOutputAPKFile(outputPath);
	bundle.addResourceSourceDir(resPath);
	bundle.setAutoAddOverlay(true);
	bundle.setAndroidManifestFile(manifestPath);
	bundle.addPackageInclude("/system/framework/framework-res.apk");
	bundle.addPackageInclude(targetApk);

	int result =  doPackage(&bundle);

	if(result != 0)
		return JNI_FALSE;
	else
		return JNI_TRUE;
}
