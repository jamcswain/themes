LOCAL_PATH := $(call my-dir)

aaptCFlags := -DAAPT_VERSION=\"$(BUILD_NUMBER_FROM_FILE)\"

include $(CLEAR_VARS)
LOCAL_MODULE := libthemeaapt
LOCAL_SRC_FILES += libaapt_jni.c libaapt.cpp
LOCAL_C_INCLUDES += frameworks/base/tools/aapt frameworks/base/libs/androidfw/include
LOCAL_SHARED_LIBRARIES := \
	libaapt \
	libandroidfw \
	libpng \
	liblog \
	libcutils \
	libexpat \
	libziparchive \
	libbase \
	libz
LOCAL_CFLAGS := -Wno-format-y2k -DSTATIC_ANDROIDFW_FOR_TOOLS $(aaptCFlags)
LOCAL_CPPFLAGS := $(aaptCFlags)

include $(BUILD_SHARED_LIBRARY)
