#include "libaapt.h"
#include "libaapt_jni.h"

JNIEXPORT jboolean JNICALL Java_com_invictrixrom_theme_LibAAPT_compileTheme(JNIEnv *env, jobject obj, jstring manifestPath, jstring resPath, jstring outputPath, jstring targetApk) {
	const char* stringManifestPath = (*env)->GetStringUTFChars(env, manifestPath, 0);
	const char* stringResPath = (*env)->GetStringUTFChars(env, resPath, 0);
	const char* stringOutputPath = (*env)->GetStringUTFChars(env, outputPath, 0);
	const char* stringTargetApk = (*env)->GetStringUTFChars(env, targetApk, 0);

	jboolean ret = compileTheme((char*) stringManifestPath, (char*) stringResPath, (char*) stringOutputPath, (char*) stringTargetApk);

	(*env)->ReleaseStringUTFChars(env, manifestPath, stringManifestPath);
	(*env)->ReleaseStringUTFChars(env, resPath, stringResPath);
	(*env)->ReleaseStringUTFChars(env, outputPath, stringOutputPath);
	(*env)->ReleaseStringUTFChars(env, targetApk, stringTargetApk);

	return ret;
}
