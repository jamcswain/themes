#ifndef __LIBAAPT_H
#define __LIBAAPT_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

jboolean compileTheme(char* manifestPath, char* resPath, char* outputPath, char* targetApk);

#ifdef __cplusplus
}
#endif

#endif
