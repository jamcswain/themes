#ifndef LIBAAPT_JNI_H
#define LIBAAPT_JNI_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     com_invictrixrom_theme_LibAAPT
 * Method:    compileTheme
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_com_invictrixrom_theme_LibAAPT_compileTheme(JNIEnv *, jobject, jstring, jstring, jstring, jstring);

#ifdef __cplusplus
}
#endif

#endif
